# terminal-duplex

```js
const Terminal = require('terminal-duplex')
const terminal = new Terminal()

x.on('some output', data=> terminal.log(data)) //console.logs
const listener = input=> terminal.log('user inputted:', input)
terminal.listen(listener) //can add as many as you want

terminal.unlisten(listener) //if you want you can also unlisten when you're done

terminal.close() //unhooks from input and outputs
```

alternatively instead of using `terminal.log` you could just pass `true` into the constructor and it will overwrite `console.log` everywhere.

```js
const Terminal = require('terminal-duplex')
const terminal = new Terminal(true)

x.on('some output', data=> console.log(data))
const listener = input=> console.log('user inputted:', input)
terminal.listen(listener) //can add as many as you want

terminal.unlisten(listener) //if you want you can also unlisten when you're done

terminal.close() //unhooks from input and outputs
```

---

# What is this?

Turns this:

![image](https://user-images.githubusercontent.com/4729968/32131678-67fe031a-bb82-11e7-80dd-98e7b1bc0a09.png)

Into this:

![image](https://user-images.githubusercontent.com/4729968/32131696-be8ba606-bb82-11e7-9846-2ce712cabf75.png)

![image](https://user-images.githubusercontent.com/4729968/32131704-d40b01ac-bb82-11e7-9070-15008c33b360.png)

![image](https://user-images.githubusercontent.com/4729968/32131711-e9a39420-bb82-11e7-9fed-281a6710c8d8.png)

Amazingly something like this didn't exist already.
